# The Open Source Packing List


## Trip Info

### Passengers

- Jon
- Pam
- Ellie

### Details

Driving to Sheboygan 10/30/21 - 11/8/21


## Dad's Computer Bag

- [ ] Dad's Laptop
- [ ] Laptop Charger
- [ ] Dad's Phone
- [ ] Phone Charger
- [ ] Mouse
- [ ] Keyboard
- [ ] Dad's Portfolio
- [ ] Blue Folder
- [ ] Plum Folder


## The GR-1

- [ ] Python Book
- [ ] External battery + cables
- [ ] Laptop & charger
- [ ] Trackball
- [ ] Magazine
- [ ] Red Folder
- [ ] Password list (in red folder)
- [ ] Blue Folder
- [ ] Pens, Pencil, & Paper
- [ ] Index cards
- [ ] Notebook
- [ ] Rain jacket
- [ ] Pill box
- [ ] Phone charger & cable
- [ ] Google Home Mini & cable
- [ ] Headlamp
- [ ] Bell & Howell flashlight
- [ ] USB charging dock
- [ ] Master Card
- [ ] Sketch binder


## On Myself

- [ ] Belt
- [ ] Pocket knife
- [ ] Shirt with collar and pocket
- [ ] Jeans
- [ ] Jacket - leather
- [ ] White socks
- [ ] Walking shoes
- [ ] Glasses
- [ ] Money clip
- [ ] Change
- [ ] Space Pen
- [ ] Handkerchief
- [ ] Cellphone
- [ ] Sears Card
- [ ] Car keys


## LSI Thermal Bag

- [ ] 2 Pepsis
- [ ] 2 PB&J sandwiches
- [ ] Carrot sticks
- [ ] Ice pack


## The Blue Duffel

- [ ] Rosary
- [ ] Flipflops
- [ ] Gloves / Hat
- [ ] Plastic bag for laundry / to sit on wet stuff
- [ ] 5 Boxershorts & briefs
- [ ] 3 pr white Socks
- [ ] 1 pr navy socks
- [ ] 4 T-Shirts
- [ ] Atletic shirt and pants
- [ ] Hoody (dark)
- [ ] 3 Sweatpants (dark)
- [ ] Dopp Kit
- [ ] Power strip
- [ ] Sock liners / outdoor socks
- [ ] Hair clippers
- [ ] Razor handle / stand / cup
- [ ] Visa Card
- [ ] Extra handkerchiefs
- [ ] Masks
- [ ] Pajama pants
- [ ] Pajama shirt
- [ ] Long sleeve athletic top
- [ ] Ball cap
- [ ] Extra cash
- [ ] Neilmed refills


## Garment Bag

- [ ] 1 pr navy pants
- [ ] 3 Flannel shirts
- [ ] 1 dressy shirt
- [ ] 3 henley/polos
- [ ] U.S. Bank Debit Card
- [ ] 1 pair jeans


## The Dopp Kit

- [ ] The Red Ditty Bag
- [ ] Eyeglass Case
- [ ] Hairgel
- [ ] Icy Hot
- [ ] Gold Bond powder
- [ ] Aloe lotion
- [ ] Extra floss
- [ ] Extra deodorant
- [ ] Prescription bottles in ziplock bag
- [ ] Tweezers and nail clippers in another ziplock bag
- [ ] Blood pressure monitor


## The Red Ditty Bag

- [ ] Listerine
- [ ] Toothbrush & paste
- [ ] Floss
- [ ] Lip balm
- [ ] Deodorant
- [ ] Shampoo & conditioner
- [ ] Soap & box
- [ ] Contact solution
- [ ] Drakkar
- [ ] Benedryl/Ibuprofen/Omeprazole in pillbox
- [ ] Singulair/Allegra in another pillbox

## Eyeglass Case

- [ ] Contacts case
- [ ] Spare contacts
- [ ] Eyeglass cloth

## Car - Front

- [ ] Paper maps
- [ ] LSI thermal bag
- [ ] Beef jerky
- [ ] Nalgene & Yeti
- [ ] Snack bag
- [ ] Red thermal bag
- [ ] Black lunch bag
- [ ] Black/brown thermal bag
- [ ] Tailbone saver
- [ ] Cheaters


## Car - Rear

- [ ] Luggage
- [ ] Dog towels
- [ ] Emergency kit
- [ ] Dog cleanup/food/bowl bag
- [ ] Beer for Gerald / Dick
- [ ] Mask
- [ ] Gloves
- [ ] Winter jacket


## Before I leave

- [ ] Check if car needs service
- [ ] Check bank account balance
- [ ] Check insurance
- [ ] Check transportation issues on route
- [ ] Check weather issues on route
- [ ] Check time difference
- [ ] Cleanup wallet. Check if credit card is in there and you know it's PIN code.
- [ ] Review photos of what's left in Sheboygan
- [ ] Beer for Feldhausen
- [ ] Prepare some off-line music & podcasts
- [ ] Shave and a haircut, so you don't have to abroad
- [ ] Take care of pending (E)Mails
- [ ] Put dog mat in back of car
- [ ] Freeze ice packs
- [ ] Arrange Phil's services
- [ ] Clean bathroom
- [ ] Set up coffee night before

## When I leave

- [ ] Unplug cords
- [ ] Grab phone charger
- [ ] Grab Google Home Mini & power cord
- [ ] Pajamas to laundry
- [ ] Put water heater in vacation mode
- [ ] Make sure baseboard heaters are off
- [ ] Log weight, pack notebook
- [ ] Make sure you grabbed nasal spray prescription
- [ ] Pack ice packs/thermal bags
- [ ] Run dishwasher
- [ ] Close toilet valve


## Tips

- Keep a spare of all toiletry items so you can just keep that bag in your suitcase.
- Nothing beats [printing](https://raw.github.com/pilsna/packing-list/master/README.md) 
and checking these with a pen ; )
- Calculations are rounded up : )

## License

Copyright (c) 2013 Kevin van Zonneveld, [http://kvz.io](http://kvz.io)  
Licensed under MIT: [http://kvz.io/licenses/LICENSE-MIT](http://kvz.io/licenses/LICENSE-MIT)

Edit over at at https://github.com/kvz/packing-list/  
Discuss over at https://news.ycombinator.com/item?id=5518382
